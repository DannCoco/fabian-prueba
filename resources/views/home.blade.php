@extends('plantilla')

@section('seccion')
<h1>Proveedores</h1>

<div <div class="container">
  <a href="{{ route('c_proveedor')}}" class="btn btn-primary"> Crear Proveedor</a>
</div>
<div <div class="container">
  <table class="table">
    <thead class="thead-dark">
      <tr>
        <th scope="col">Nit</th>
        <th scope="col">Nombre</th>
        <th scope="col">Direccion</th>
        <th scope="col">Telefono</th>
        <th scope="col">Id Producto</th>
        <th scope="col">Estado</th>
      </tr>
    </thead>
    <tbody>
      @foreach($proveedores as $proveedor)

      <tr>
        <th scope="row">{{$proveedor->Nit}}</th>
        <td>{{$proveedor->Nombre}}</td>
        <td>{{$proveedor->Direccion}}</td>
        <td>{{$proveedor->Telefono}}</td>
        <td>{{$proveedor->Id_producto}}</td>
        <td>{{$proveedor->Estado}}</td>
      </tr>

      @endforeach()

    </tbody>
  </table>
</div>


@endsection
