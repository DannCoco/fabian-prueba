<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Proveedor extends Model
{
    protected $table = 'provider';


    protected $primaryKey = 'id_provider';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['nit', 'nombre', 'direccion', 'telefono'];

    public static function providers()
    {
        $query = self::query();
        return $query->get();
    }
}
