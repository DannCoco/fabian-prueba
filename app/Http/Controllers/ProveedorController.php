<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Proveedor;

class ProveedorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $providers = Proveedor::providers();
        return view('provider.index', compact('providers'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('provider.create-edit');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    { 
        try {
            $data = $request->all();
            $provider = new Proveedor;
            $provider->fill($data);
            $provider->estado = 'Activo';
            $provider->save();
    
            return view('provider.create-edit');
        
        } catch (\Exception $e) {
            \Log::error(sprintf('%s:%s', 'ProveedorController:store', $e->getMessage()));
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Proveedor  $Proveedor
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Proveedor  $Proveedor
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(!is_numeric($id)){
            throw new \Exception("Este valor no es numérico", $id);            
        }

        $provider = Proveedor::find($id);
        if(!$provider instanceof Proveedor){
            throw new \Exception("Este proveedor no exite en la base de datos", $id);            
        }

        return view('provider.create-edit', compact('provider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Proveedor  $Proveedor
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        try {
            $data = $request->all();
            $provider = Proveedor::find($id);
            $provider->fill($data);
            $provider->save();
    
            return view('provider.create-edit', compact('provider'));
            
        } catch (\Exception $e) {
            \Log::error(sprintf('%s:%s', 'ProveedorController:update', $e->getMessage()));
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Proveedor  $Proveedor
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(!is_numeric($id)){
            throw new \Exception("Este valor no es numérico", $id);            
        }

        $provider = Proveedor::find($id);
        if(!$provider instanceof Proveedor){
            throw new \Exception("Este proveedor no exite en la base de datos", $id);            
        }

        $provider->delete();

        return redirect('/provider');
    }
}
