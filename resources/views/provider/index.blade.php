@extends('admin.layouts.app')
@section('content')
<section class="content">
  <div class="row">
    <div class="col-md-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Proveedores</h3>
        </div>
        <div>
          <a href="{{ route('provider.create') }}" class="btn btn-primary btn-flat">
            Crear proveedor
          </a>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <table class="table table-bordered">
            <tbody>
              <tr>
                <th style="width: 10px">#</th>
                <th>Nombre</th>
                <th>Dirección</th>
                <th>Teléfono</th>
                <th>Nit</th>
                <th>Acción</th>
              </tr>
              <tr>
                @foreach($providers as $provider)
                <td>{{ $provider->id_provider }}</td>
                <td>{{ $provider->nombre }}</td>
                <td>{{ $provider->direccion }}</td>
                <td>{{ $provider->telefono }}</td>
                <td>{{ $provider->nit }}</td>
                <td>
                  <a class="btn btn-primary btn-flat" href="{{ route('provider.edit', $provider->id_provider) }}"><i class="fa fa-pencil"></i></a>
                  <form action="{{ route('provider.destroy', $provider->id_provider) }}" method="post">
                      {{ method_field('delete') }}
                      <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
                      <button class="btn btn-danger btn-flat" type="submit"><i class="fa fa-trash"></i></button>
                  </form>
                </td>
                @endforeach
              </tr>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer clearfix">
          <ul class="pagination pagination-sm no-margin pull-right">
            <li><a href="#">«</a></li>
            <li><a href="#">1</a></li>
            <li><a href="#">2</a></li>
            <li><a href="#">3</a></li>
            <li><a href="#">»</a></li>
          </ul>
        </div>
      </div>
      <!-- /.box -->
    </div>
  </div>
  </div>
  @endsection
