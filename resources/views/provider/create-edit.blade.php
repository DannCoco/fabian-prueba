@extends('admin.layouts.app')
@section('content')
<div class="row">
  <!-- left column -->
  <div class="col-md-12">
    <!-- general form elements -->
    <div class="box box-primary">
      <div class="box-header with-border">
        <h3 class="box-title">Crear proveedor</h3>
      </div>
      <!-- /.box-header -->
      <!-- form start -->
      <form role="form" action="{{ isset($provider) ? route('provider.update', $provider->id_provider) : route('provider.store') }}" method="POST">
        @if(isset($provider))
          @method('PUT')
        @endif
        <input type="hidden" name="_token" id="csrf-token" value="{{ Session::token() }}" />
        <div class="box-body">
          <div class="row">
            <!-- left column -->
            <div class="col-md-6">
              <div class="form-group">
                <label for="provider_name">Nombre</label>
                <input type="text" name="nombre" class="form-control" value="{{ isset($provider) ? $provider->nombre : '' }}" id="provider_name" placeholder="Nombre">
              </div>
            </div>
            <!-- left column -->
            <div class="col-md-6">
              <div class="form-group">
                <label for="provider_phone">Teléfono</label>
                <input type="text" name="telefono" class="form-control" value="{{ isset($provider) ? $provider->telefono : '' }}" id="provider_phone" placeholder="Teléfono">
              </div>
            </div>
          </div>
          <div class="row">
             <!-- left column -->
            <div class="col-md-6">
              <div class="form-group">
                <label for="provider_direction">Dirección</label>
                <input type="text" name="direccion" class="form-control" value="{{ isset($provider) ? $provider->direccion : '' }}" id="provider_direction" placeholder="Nombre">
              </div>
            </div>
            <!-- left column -->
            <div class="col-md-6">
              <div class="form-group">
                <label for="provider_nit">NIT</label>
                <input type="text" name="nit" class="form-control" value="{{ isset($provider) ? $provider->nit : '' }}" id="provider_nit" placeholder="Teléfono">
              </div>
            </div>
          </div>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">
          <button type="submit" class="btn btn-primary">Guardar</button>
        </div>
      </form>
    </div>
    <!-- /.box -->
  </div>
</div>
@endsection
